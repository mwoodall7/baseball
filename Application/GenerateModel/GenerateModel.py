'''
Copyright © Matthew Woodall (mwoodall7 on Github). All Worldwide Rights Reserved.
This material is the property of Matthew Woodall a.k.a. mwoodall7 on Github.
All use, alterations, disclosure, dissemination, and/or reproduction not specifically
authorized by mwoodall7 is prohibited.

'''

def main():
    pass


if __name__ == '__main__':
    main()

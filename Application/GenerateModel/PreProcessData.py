import numpy as np
import os
import csv
from inspect import currentframe, getframeinfo

FILE_INFO = getframeinfo(currentframe())
__name__ = os.path.basename(FILE_INFO.filename)


class PreProcessData:

    def __init__(self, dataPath):

        self.data_file_path = dataPath
        self.expected_number_of_data_points = 862
        self.need_additional_zeros = 842


    def dataGenerator(self):
        for artifact in sorted(os.listdir(self.data_file_path)):
            yield self.readFileData(os.path.join(self.data_file_path, artifact))


    def readFileData(self, filePath):
        gameArray = np.array([], ndmin=1)
        try:
            with open(filePath) as f:
                fileReader = csv.reader(f, delimiter=' ')
                for row in fileReader:
                    for val in row:
                        gameArray = np.append(gameArray, float(val))
            numDataPoints = len(gameArray)
            if numDataPoints != self.expected_number_of_data_points:
                if numDataPoints == self.need_additional_zeros:
                    print(f"{__name__}.{FILE_INFO.lineno}>> CSV File {os.path.basename(filePath)} missing data. Padding array.")
                    for _ in range(20):
                        gameArray = np.append(gameArray, 0)
                else:
                    print(f"{__name__}.{FILE_INFO.lineno}>> CSV File {os.path.basename(filePath)} invalid. Expected Data Points: {self.expected_number_of_data_points}, Received Data Points: {numDataPoints}")
                    gameArray = np.array([], ndmin=1)
        except FileNotFoundError as e:
            pass
        return gameArray

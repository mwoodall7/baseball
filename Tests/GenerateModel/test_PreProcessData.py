import unittest
import os
import numpy as np
from types import GeneratorType

from Application.GenerateModel.PreProcessData import PreProcessData


class TestPreProcessData(unittest.TestCase):

    def setUp(self):
        self.data_file_path = os.path.join(os.getcwd(), "Tests", "GenerateModel", "PracticeData")
        self.example_csv_file_path = os.path.join(self.data_file_path, "0_1_Arizona Diamondbacks_Boston Red Sox_2019-04-07.csv")
        self.fake_csv_file_path = os.path.join(self.data_file_path, "not_a_file.csv")
        self.bad_csv_file_path = os.path.join(self.data_file_path, "garbage_Data.csv")
        self.pre_process_data_object = PreProcessData(self.data_file_path)
        self.practice_pre_process_data_object = PreProcessData(self.data_file_path)

        self.example_csv_file_data_array = np.array([5.0, 5.0, 3.0, 8.0, 890.0, 872.0, 559.0, 370.0, 76.0, 36.0, 21.0, 445.0, 232.0, 8.0, 584.0, 21.0, 0.246, 2378.0, 0.316, 0.334, 0.651, 44.0, 256.0, 0.853, 30.0, 10048.0, 2684.0, 795.0, 175.0, 877.0, 39.0, 14.0, 0.292, 1.56, 113.24, 45.0, 1430.0, 25.0, 1500.0, 0.983, 2.25, 4803.0, 656.0, 505.0, 17.0, 982.0, 819.0, 1049.0, 426.0, 189.0, 32.0, 110.0, 712.0, 245.0, 21.0, 862.0, 21.0, 0.257, 3359.0, 0.308, 0.43, 0.739, 15.0, 20.0, 0.571, 50.0, 14139.0, 3671.0, 1445.0, 446.0, 1429.0, 13.0, 33.0, 0.293, 0.78, 30.54, 787.0, 249.0, 34.0, 1070.0, 0.968, 2.06, 4023.0, 502.0, 446.0, 93.0, 145.0, 71.0, 87.0, 59.0, 17.0, 3.0, 3.0, 63.0, 24.0, 0.0, 75.0, 27.0, 0.256, 293.0, 0.365, 0.365, 0.73, 0.0, 26.0, 1.0, 1.0, 1356.0, 347.0, 107.0, 24.0, 115.0, 2.0, 1.0, 0.316, 0.82, 97.67, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1823.0, 2115.0, 1795.0, 963.0, 336.0, 29.0, 282.0, 1395.0, 340.0, 20.0, 1939.0, 103.0, 0.277, 7009.0, 0.317, 0.454, 0.771, 35.0, 97.0, 0.735, 190.0, 27077.0, 7516.0, 3179.0, 945.0, 2801.0, 8.0, 55.0, 0.308, 1.18, 24.86, 4.0, 281.0, 7.0, 292.0, 0.976, 1.69, 1333.2, 169.0, 150.0, 1.0, 270.0, 188.0, 196.0, 130.0, 48.0, 2.0, 42.0, 245.0, 94.0, 6.0, 211.0, 12.0, 0.253, 835.0, 0.335, 0.466, 0.801, 2.0, 10.0, 0.833, 18.0, 3798.0, 945.0, 389.0, 116.0, 381.0, 0.0, 4.0, 0.306, 0.96, 19.88, 171.0, 1411.0, 16.0, 1598.0, 0.99, 7.87, 1640.1, 201.0, 184.0, 146.0, 591.0, 690.0, 551.0, 294.0, 122.0, 29.0, 56.0, 350.0, 185.0, 8.0, 607.0, 14.0, 0.282, 2149.0, 0.341, 0.444, 0.785, 13.0, 39.0, 0.75, 35.0, 8939.0, 2371.0, 955.0, 236.0, 808.0, 6.0, 17.0, 0.313, 1.25, 38.38, 521.0, 335.0, 8.0, 864.0, 0.991, 3.31, 1816.0, 259.0, 193.0, 135.0, 670.0, 699.0, 600.0, 277.0, 112.0, 20.0, 60.0, 456.0, 167.0, 11.0, 528.0, 12.0, 0.239, 2213.0, 0.293, 0.389, 0.681, 18.0, 29.0, 0.617, 54.0, 8884.0, 2426.0, 860.0, 260.0, 913.0, 11.0, 23.0, 0.272, 1.17, 36.88, 1786.0, 803.0, 59.0, 2648.0, 0.978, 4.01, 5406.2, 646.0, 600.0, 395.0, 284.0, 156.0, 178.0, 69.0, 32.0, 1.0, 18.0, 221.0, 43.0, 1.0, 146.0, 2.0, 0.215, 680.0, 0.262, 0.344, 0.606, 0.0, 0.0, 0.0, 10.0, 2887.0, 737.0, 234.0, 61.0, 312.0, 7.0, 5.0, 0.287, 0.88, 37.78, 111.0, 1566.0, 13.0, 1690.0, 0.992, 6.71, 1669.2, 250.0, 33.0, 15.0, 174.0, 8.0, 82.0, 0.713, 30.0, 26.0, 6.0, 3.0, 0.0, 0.0, 0.0, 25.0, 2.0, 0.0, 1.0, 0.0, 0.019, 52.0, 0.056, 0.019, 0.075, 0.0, 0.0, 0.0, 2.0, 206.0, 60.0, 1.0, 2.0, 39.0, 6.0, 0.0, 0.037, 4.33, 0.0, 17.0, 28.0, 4.0, 49.0, 0.918, 1.22, 214.2, 37.0, 37.0, 2.0, 37.0, 37.0, 223.0, 215.0, 104.0, 44.0, 4.0, 34.0, 187.0, 62.0, 5.0, 210.0, 3.0, 0.254, 828.0, 0.306, 1.089, 1.396, 2.0, 6.0, 0.75, 17.0, 3435.0, 4.15, 214.2, 16.0, 16.0, 0.0, 0.0, 0.0, 99.0, 1.27, 902.0, 37.0, 0.0, 0.0, 2206.0, 64.2, 3.0, 0.0, 4.0, 0.0, 1.04, 0.5, 16.0, 0.0, 3.02, 7.84, 2.6, 8.8, 4.49, 1.43, 0.0, 0.0, 2.0, 5.0, 485.0, 477.0, 512.0, 279.0, 119.0, 13.0, 51.0, 400.0, 221.0, 9.0, 502.0, 17.0, 0.273, 1837.0, 0.353, 0.435, 0.789, 13.0, 53.0, 0.803, 32.0, 8253.0, 2104.0, 800.0, 260.0, 724.0, 8.0, 21.0, 0.321, 0.93, 36.02, 29.0, 630.0, 11.0, 670.0, 0.984, 1.55, 3618.0, 425.0, 406.0, 5.0, 970.0, 1051.0, 965.0, 578.0, 236.0, 14.0, 118.0, 756.0, 342.0, 16.0, 1083.0, 31.0, 0.289, 3748.0, 0.351, 0.454, 0.805, 14.0, 61.0, 0.813, 87.0, 16769.0, 4155.0, 1701.0, 531.0, 1576.0, 5.0, 29.0, 0.332, 1.09, 31.76, 2209.0, 1161.0, 78.0, 3448.0, 0.977, 3.71, 7971.1, 908.0, 896.0, 459.0, 1179.0, 1130.0, 949.0, 499.0, 208.0, 8.0, 176.0, 917.0, 367.0, 34.0, 968.0, 33.0, 0.252, 3842.0, 0.32, 0.448, 0.767, 5.0, 11.0, 0.688, 92.0, 16877.0, 4284.0, 1720.0, 588.0, 1749.0, 6.0, 36.0, 0.284, 1.19, 21.83, 562.0, 7403.0, 34.0, 7999.0, 0.996, 7.67, 8354.0, 1038.0, 923.0, 773.0, 392.0, 416.0, 357.0, 254.0, 108.0, 5.0, 74.0, 364.0, 117.0, 16.0, 433.0, 7.0, 0.279, 1551.0, 0.332, 0.498, 0.83, 11.0, 16.0, 0.593, 30.0, 6336.0, 1680.0, 773.0, 254.0, 651.0, 1.0, 4.0, 0.321, 1.17, 20.96, 728.0, 232.0, 74.0, 1034.0, 0.928, 2.52, 3352.1, 381.0, 375.0, 58.0, 234.0, 164.0, 145.0, 98.0, 28.0, 4.0, 12.0, 190.0, 52.0, 0.0, 155.0, 2.0, 0.243, 639.0, 0.301, 0.355, 0.656, 4.0, 10.0, 0.714, 14.0, 2749.0, 696.0, 227.0, 67.0, 289.0, 2.0, 1.0, 0.326, 1.13, 53.25, 54.0, 846.0, 6.0, 906.0, 0.993, 6.98, 960.0, 129.0, 23.0, 20.0, 106.0, 5.0, 63.0, 0.733, 858.0, 936.0, 803.0, 353.0, 148.0, 19.0, 58.0, 400.0, 135.0, 6.0, 776.0, 16.0, 0.276, 2813.0, 0.31, 0.404, 0.714, 38.0, 142.0, 0.789, 64.0, 10618.0, 3008.0, 1136.0, 309.0, 1160.0, 20.0, 24.0, 0.302, 1.17, 48.5, 264.0, 187.0, 11.0, 462.0, 0.976, 2.82, 1190.2, 160.0, 132.0, 55.0, 873.0, 822.0, 623.0, 435.0, 162.0, 23.0, 98.0, 828.0, 304.0, 17.0, 692.0, 54.0, 0.239, 2899.0, 0.321, 0.412, 0.732, 14.0, 60.0, 0.811, 48.0, 12905.0, 3281.0, 1194.0, 376.0, 1299.0, 5.0, 19.0, 0.298, 1.32, 29.58, 54.0, 1783.0, 17.0, 1854.0, 0.991, 2.35, 6754.1, 781.0, 752.0, 18.0, 849.0, 798.0, 1160.0, 660.0, 238.0, 27.0, 155.0, 502.0, 395.0, 26.0, 1029.0, 21.0, 0.301, 3422.0, 0.373, 0.522, 0.895, 27.0, 136.0, 0.834, 43.0, 15496.0, 3875.0, 1786.0, 509.0, 1102.0, 4.0, 33.0, 0.312, 0.69, 22.08, 36.0, 1383.0, 14.0, 1433.0, 0.99, 2.23, 5514.0, 637.0, 620.0, 5.0, 7.0, 2.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 3.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 11.0, 3.0, 0.0, 0.0, 4.0, 0.0, 0.0, 0.0, 2.0, 0.0, 16.0, 12.0, 2.0, 30.0, 0.933, 0.31, 166.0, 89.0, 19.0, 3.0, 89.0, 19.0, 183.0, 162.0, 83.0, 35.0, 4.0, 18.0, 121.0, 61.0, 1.0, 176.0, 10.0, 0.277, 635.0, 0.347, 1.123, 1.47, 8.0, 5.0, 0.385, 20.0, 2752.0, 3.9, 166.0, 11.0, 7.0, 0.0, 1.0, 4.0, 72.0, 1.43, 713.0, 89.0, 0.0, 0.0, 1740.0, 63.2, 10.0, 0.0, 6.0, 0.0, 1.13, 0.611, 16.6, 30.0, 1.98, 6.56, 3.31, 9.54, 4.72, 0.98, 27.0, 11.0, 1.0, 5.0])

        self.expected_number_of_data_points = 862
        self.empty_np_array = np.array([], ndmin=1)


################################################################################
# readFileData - pass in the file path of a csv file and return a 1-D numpy array
# of the data set
#
# Use Cases:
# 1. valid file passed in. return a valid size np array with correct data
# 2. valid file passed in but file does not contain enough data. return empty np
#    array
# 3. invalid file passed in. return empty np array
################################################################################

    def test_PreProcessData_readFileData_validFile(self):
        goodData = self.pre_process_data_object.readFileData(self.example_csv_file_path)
        self.assertEqual(self.example_csv_file_data_array.all(), goodData.all())
        self.assertEqual(self.expected_number_of_data_points, len(goodData))


    def test_PreProcessData_readFileData_validFileBadData(self):
        badData = self.pre_process_data_object.readFileData(self.bad_csv_file_path)
        self.assertEqual(self.empty_np_array.all(), badData.all())
        self.assertEqual(0, len(badData))


    def test_PreProcessData_readFileData_invalidFile(self):
        badData = self.pre_process_data_object.readFileData(self.fake_csv_file_path)
        self.assertEqual(self.empty_np_array.all(), badData.all())
        self.assertEqual(0, len(badData))


################################################################################
# dataGenerator - return a generator that yields the numpy array of data per
# file when called
#
# Use Cases:
# 1. calling the data generator results in a generator object that will yield
#    game data
# 2. yields proper game data for good file
# 3. yields empty array for bad file
################################################################################

    def test_PreProcessData_dataGenerator_yieldGen(self):
        gen = self.practice_pre_process_data_object.dataGenerator()
        self.assertIsInstance(gen, GeneratorType)


    def test_PreProcessData_dataGenerator_goodFile(self):
        gen = self.practice_pre_process_data_object.dataGenerator()
        goodData = gen.__next__()
        self.assertEqual(self.example_csv_file_data_array.all(), goodData.all())
        self.assertEqual(self.expected_number_of_data_points, len(goodData))


    def test_PreProcessData_dataGenerator_badFile(self):
        gen = self.practice_pre_process_data_object.dataGenerator()
        goodData = gen.__next__()
        self.assertEqual(self.example_csv_file_data_array.all(), goodData.all())
        self.assertEqual(self.expected_number_of_data_points, len(goodData))
        goodData2 = gen.__next__()
        self.assertEqual(self.expected_number_of_data_points, len(goodData2))
        goodData3 = gen.__next__()
        self.assertEqual(self.expected_number_of_data_points, len(goodData3))
        goodData4 = gen.__next__()
        self.assertEqual(self.expected_number_of_data_points, len(goodData4))
        badData = gen.__next__()
        self.assertEqual(self.empty_np_array.all(), badData.all())
        self.assertEqual(0, len(badData))


if __name__ == '__main__':
    unittest.main()
